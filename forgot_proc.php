<?php

require_once "config.php";
$directory = ROOT_PATH . DIRECTORY_SEPARATOR;
require_once "functions.php";

if ($_POST['action'] == 'Remember') { //If remembering login and password by email
    if (!empty($_POST['email'])) {
        $_SESSION['email'] = $_POST['email'];
        $login = checkEmail($pdo, $_POST['email']);
        if (!empty($login)) {//If user was finded
            $_SESSION['login'] = $login;
            //Moving to the forgot.php
            header("location: /hw_20/forgot.php");
        } else {
            //User with this email isn't exists
            $_SESSION['errors'][] = 3;
            //Moving to the forgot page
            header("location: /hw_20/forgot.php");
            die();
        }
    } else {
        session_unset();
        //Field for email is empty
        $_SESSION['errors'][] = 4;
        //Moving to the forgot page
        header("location: /hw_20/forgot.php");
        die();
    }
} elseif ($_POST['action'] == 'Login') {
    $_SESSION['newPassword'] = $_POST['newPassword'];
    $_SESSION['confirmPassword'] = $_POST['confirmPassword'];
    if (!empty($_SESSION['login'])) {
        //Checking length of password (it must be from 7 to 13 symbols)
        if ((strlen($_SESSION['newPassword']) > 6) && (strlen($_SESSION['newPassword']) < 14)) {
            if (!empty($_POST['newPassword']) && !empty($_POST['confirmPassword'])) {
                if ($_SESSION['newPassword'] != $_SESSION['confirmPassword']) {
                    //Password wasn't confirm.
                    $_SESSION['errors'][] = 7;
                    //Moving to the forgot page
                    header("location: /hw_20/forgot.php");
                    die();
                } else {
                    saveNewPassword($pdo, $_SESSION['login'], $_SESSION['newPassword']);
                    $_SESSION['password'] = $_SESSION['newPassword'];
                }

                //Finding login and checking the password
                $userData = checkLoginPsw($pdo, $_SESSION['login'], $_SESSION['password']);
                if (!empty($userData)) {//If user was finded
                //Incrementing number of accesses for finded user
                    $userData = accessStatistics($pdo, $userData, $_SESSION['login'], $_SESSION['password']);

                    //var_dump($userData);

                    $_SESSION['userData'] = $userData;
                    //$_SESSION['name'] = $userData['name'];
                    //$_SESSION['lang'] = $userData['lang'];
                    if (isset($_POST['remember'])) { //Setting the cookies
                        $hour = time() + 3600 * 24 * 30;
                        setcookie('remember', $_POST['remember'], $hour);
                        setcookie('login', $_SESSION['login'], $hour);
                        setcookie('password', $_SESSION['password'], $hour);
                    } else { //Unsetting the cookies
                        $hour = time() - 3600;
                        setcookie('remember', "", $hour);
                        setcookie("login", "", $hour);
                        setcookie("password", "", $hour);
                    }

                    //Moving to the site page
                    header("location: /hw_20/site_page.php");
                    die();
                    //header('Location: /API/site_page.php');
                } else { //If login or password is wrong
                    $_SESSION['errors'][] = 1;
                    //Moving to the forgot page
                    header("location: /hw_20/forgot.php");
                    die();
                }
            } else { //if New Password or Confirm Password is empty
                //Field for new password or confirm password is empty.
                $_SESSION['errors'][] = 6;
                //Moving to the forgot page
                header("location: /hw_20/forgot.php");
                die();
            }
        } else {
            //The length of password must be from 7 to 13 symbols
            $_SESSION['errors'][] = 8;
            //Moving to the forgot page
            header("location: /hw_20/forgot.php");
            die();
        }
    } else {
        //User by email wasn't finded
        $_SESSION['errors'][] = 5;
        //Moving to the forgot page
        header("location: /hw_20/forgot.php");
        die();
    }
} elseif ($_POST['action'] == 'Exit') {
    session_unset();
    require $directory . "entrance.php";
} else {

}