<?php
require_once "config.php";

$hour = time() - 3600;
setcookie('remember', "", $hour);
setcookie("login", "", $hour);
setcookie("password", "", $hour);
//$_SESSION['login'] = "";
//$_SESSION['password'] = "";

?>

<!DOCTYPE>
<html>
<head>
    <meta charset="utf-8">
    <title>Site entrance</title>
</head>
<body>
    <form action = "/hw_20/forgot_proc.php" method = "post">
        <p>
            <label>Email</label>
            <input type = "text" name = "email" value = 
            <?php if (isset($_SESSION['email'])) { 
                echo $_SESSION['email'];
            } else { ?>
                ""
            <?php 
                $_SESSION['login'] = "";
                $_SESSION['password'] = "";
        } ?> />
        </p>
        <div>
            <input type = "submit" name = "action" value = "Remember" />
        </div>
        <p>
            <label>Login ></label>
            <output name="login">
                <?php if (isset($_SESSION['login'])) { 
                echo $_SESSION['login'];
                } else { ?>
                    ""
                <?php } ?>
            </output>
            
        </p>
        <p>
            <label>New Password</label>
            <input type = "password" name = "newPassword" value = 
             <?php if (isset($_SESSION['newPassword'])) { 
                echo $_SESSION['newPassword'];
            } else { ?>
                ""
            <?php } ?> >
        </p>
        <p>
            <label>Confirm Password</label>
            <input type = "password" name = "confirmPassword" value = 
             <?php if (isset($_SESSION['confirmPassword'])) { 
                echo $_SESSION['confirmPassword'];
            } else { ?>
                ""
            <?php } ?> >
        </p>
        <div><input type="checkbox" name="remember" id="remember" 
                <?php if(isset($_SESSION["remember"])) { ?> checked <?php } ?> />
            <label for="remember-me">Remember me</label>
        </div>
        <div>
            <input type = "submit" name = "action" value = "Login" />
            <input type = "submit" name = "action" value = "Exit" />
        </div>
    </form>

    <?php if (!empty($_SESSION['errors'])) {
        errorsProc($_SESSION['errors']);
        unset($_SESSION['errors']);
    } ?>

</body>
</html>