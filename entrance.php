<?php
require_once "config.php";
?>

<!DOCTYPE>
<html>
<head>
    <meta charset="utf-8">
    <title>Site entrance</title>
</head>
<body>
    <form action = "login_check.php" method = "post">
        <p>
            <label>Login</label>
            <input type = "text" name = "login" value = 
            <?php if (isset($_COOKIE['login'])) { 
                echo $_COOKIE['login'];
            } elseif (isset($_SESSION['login'])) {
                echo $_SESSION['login'];
            } else { ?>
                ""
            <?php } ?> >
        </p>
        <p>
            <label>Password</label>
            <input type = "password" name = "password" value = 
             <?php if (isset($_COOKIE['password'])) { 
                echo $_COOKIE['password'];
            } elseif (isset($_SESSION['password'])) {
                echo $_SESSION['password'];
            } else { ?>
                ""
            <?php } ?> >
        </p>
        <div><input type="checkbox" name="remember" id="remember" 
                <?php if(isset($_COOKIE["remember"])) { ?> checked <?php } ?> />
            <label for="remember-me">Remember me</label>
        </div>
        <p>
            <input type = "submit" name="action" value = "Login" />
            <span class="psw">Forgot <a href="/hw_20/forgot.php">password?</a></span>
        </p>
        <div>
            <input type = "submit" name="action" value = "Registration" /> 
        </div>
    </form>

    <?php if (!empty($_SESSION['errors'])) {
        errorsProc($_SESSION['errors']);
        unset($_SESSION['errors']);
    } ?>

</body>
</html>