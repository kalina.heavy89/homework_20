<?php
require_once "config.php";
$directory = ROOT_PATH . DIRECTORY_SEPARATOR;
require_once "functions.php";

$_SESSION['name'] = $_POST['name'];
$_SESSION['login'] = $_POST['login'];
$_SESSION['password'] = $_POST['password'];
$_SESSION['confirmPassword'] = $_POST['confirmPassword'];
$_SESSION['email'] = $_POST['email'];
$_SESSION['lang'] = $_POST['lang'];

//Checking that all needed fields was written
if ((empty($_POST['name'])) || (empty($_POST['login'])) || (empty($_POST['password'])) || (empty($_POST['confirmPassword'])) || (empty($_POST['email']))) {
    //All fields marked with an asterisk must be written.
    $_SESSION['errors'][] = 9;
    //Moving to the forgot page
    header("location: /hw_20/register.php");
    die();
}

//Checking that confirmPassword repeates password
if ($_SESSION['password'] != $_SESSION['confirmPassword']) {
    //Password wasn't confirmed.
    $_SESSION['errors'][] = 7;
    //Moving to the forgot page
    header("location: /hw_20/register.php");
    die();
}

//Checking length of password (it must be from 7 to 13 symbols)
if ((strlen($_SESSION['password']) < 7) || (strlen($_SESSION['password']) > 13)) {
    //The length of password must be from 7 to 13 symbols.
    $_SESSION['errors'][] = 8;
    //Moving to the forgot page
    header("location: /hw_20/register.php");
    die();
}

//Checking that fields ['login'] and ['email'] nowhere repeated
if (findUserId ($pdo, 'login', $_SESSION['login'])) {
    //User with this login is exists
    $_SESSION['errors'][] = 10;
    //Moving to the forgot page
    header("location: /hw_20/register.php");
    die();
} elseif (findUserId ($pdo, 'email', $_SESSION['email'])) {
    //User with this email is exists
    $_SESSION['errors'][] = 11;
    //Moving to the forgot page
    header("location: /hw_20/register.php");
    die();
} else {;}

//Creating array with user's data
$userData = 
    [
        "name" => $_SESSION['name'], 
        "login" => $_SESSION['login'], 
        "password" =>  encryptPass($_SESSION['password']), 
        "email" => $_SESSION['email'], 
        "lang" => $_SESSION['lang'],
        "accessNum" => 0
    ];

//Writing new user's data to table `users`
addUserToTable($userData, $pdo);

//Establish number of accesses of new user
$userData = accessStatistics($pdo, $userData, $_SESSION['login'], $_SESSION['password']);

$_SESSION['userData'] = $userData;

//Entering into the site
if (isset($_POST['remember'])) { //Setting the cookies
    $hour = time() + 3600 * 24 * 30;
    setcookie('remember', $_POST['remember'], $hour);
    setcookie('login', $_POST['login'], $hour);
    setcookie('password', $_POST['password'], $hour);
} else { //Unsetting the cookies
    $hour = time() - 3600;
    setcookie('remember', "", $hour);
    setcookie("login", "", $hour);
    setcookie("password", "", $hour);
}

//Moving to the site page
header("location: /hw_20/site_page.php");
die();