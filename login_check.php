<?php
require_once "config.php";
$directory = ROOT_PATH . DIRECTORY_SEPARATOR;
require_once "functions.php";
//define(SALT1, "&ds56sYG41g%");
//define(SALT2, "54sd5SdV");

//var_dump($_POST);
//var_dump($_COOKIE);
//die();

//Reading file that consists of users data
//$users = openCreateUsersJson('users.json', 'users.txt');
$users = writeUserDataToTable('users.json', 'users.txt', 'users', $pdo);

if (($_POST['action'] == 'Login')) {
    $_SESSION['login'] = $_POST['login'];
    $_SESSION['password'] = $_POST['password'];
    if (!empty($_POST['login']) && !empty($_POST['password'])) {
        //Finding login and checking the password
        $userData = checkLoginPsw($pdo, $_SESSION['login'], $_SESSION['password']);
        if (!empty($userData)) {//If user was finded
            //Incrementing number of accesses for finded user
            $userData = accessStatistics($pdo, $userData, $_SESSION['login'], $_SESSION['password']);

            //var_dump($userData);

            $_SESSION['userData'] = $userData;
            //$_SESSION['name'] = $userData['name'];
            //$_SESSION['lang'] = $userData['lang'];
            if (isset($_POST['remember'])) { //Setting the cookies
                $hour = time() + 3600 * 24 * 30;
                setcookie('remember', $_POST['remember'], $hour);
                setcookie('login', $_SESSION['login'], $hour);
                setcookie('password', $_SESSION['password'], $hour);
            } else { //Unsetting the cookies
                $hour = time() - 3600;
                setcookie('remember', "", $hour);
                setcookie("login", "", $hour);
                setcookie("password", "", $hour);
            }

            //Moving to the site page
            header("location: /hw_20/site_page.php");
            die();
            //header('Location: /API/site_page.php');
        } else { //If login or password is wrong
            //session_unset();
            //wrongLoginPassword ();
            $_SESSION['errors'][] = 1;
            //Moving to the site page
            header("location: /hw_20/entrance.php");
            die();
        } 
    } else { //if login or password is empty
        //emptyLoginPassword ();
        $_SESSION['errors'][] = 2;
        header("location: /hw_20/entrance.php");
        die();
    }
} elseif ($_POST['action'] == 'Registration') {
    header("location: /hw_20/register.php");
    die();
} elseif ($_POST['action'] == 'Exit') {
    session_unset();
    header("location: /hw_20/entrance.php");
    die();
    //require $directory . "entrance.php";
} elseif ($_POST['action'] == 'Delete') {
    deleteUser ($pdo, $_SESSION['login']);
    //unset($_COOKIE['remember']);
    //unset($_COOKIE['login']);
    //unset($_COOKIE['password']);
    session_unset();
    deleteAllCookies();
    //require $directory . "entrance.php";       
    header("location: /hw_20/entrance.php");
    die();
} else {
    ;
}