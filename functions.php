<?php
declare(strict_types = 1);
require_once "config.php";

define("SALT", 'dfbjnlfg5464f54');

function encryptPass(string $password): string
{   
    return sha1($password . SALT);
}

//Function for writing array to json-file
function arrayToJson (string $fileJson, array $array)
{
    $directory = ROOT_PATH . DIRECTORY_SEPARATOR;
    file_put_contents($directory . $fileJson , json_encode($array));
}

//Function that creates data base from array
function createDataBase (array $array, array $keys): array
{
    $i = 0;
    $j = 0;
    $k = 0;
    $sizeKeys = count($keys);
    foreach ($array as $val) {
        if ($keys[$j] == "password") {
            $array[$i] = encryptPass($array[$i]);
        }
        $dataBase[$k][$keys[$j++]] = $array[$i++];
        if ($j == $sizeKeys) {
            $j = 0;
            $k++;
        }
    }
    return $dataBase;
}

//Function for opening and creating (if file isn't exists) json-file with user's data
function openCreateUsersJson(string $fileJson, string $blankTxt): array
{
    $directory = ROOT_PATH . DIRECTORY_SEPARATOR;
    if (! file_exists($directory . $fileJson)) {
        //Reading text file
        $fileString = file_get_contents($directory . $blankTxt);
        //Replacing symbols \t\n\r\0\x0B to space 
        $replSymbols = ["\t", "\r", "\n", "\0", "\x0B"];
        $fileString = str_replace($replSymbols, " ", $fileString); 
        //Deleting repeated spaces
        $fileString = preg_replace('/^ +| +$|( ) +/m', '$1', $fileString);
        //echo $fileString . "<br>";
        //Exploding string into array
        $fileArray = explode(" ", $fileString);
        //Creating users array
        $arrayKeys = ["id", "name", "login", "password", "email", "lang", "accessNum"];

        $array = createDataBase ($fileArray, $arrayKeys);

        arrayToJson ($fileJson, $array);
    }
        
    $usersJson = file_get_contents($directory . $fileJson);
    $array = json_decode($usersJson, true);
    return $array;
}

function addUserToTable(array $userData, object $connection): bool
{
    $stmt = $connection->prepare("
        INSERT INTO `users`(
            `name`, 
            `email`, 
            `login`, 
            `password`,
            `lang`,
            `accessNum`
        )
        VALUES (
            :name, 
            :email, 
            :login, 
            :password,
            :lang,
            :accessNum
        )
    ");
    //Field `id` in table `products` must be AUTO INCREMENTed
    return $stmt->execute(
        [
            "name" => $userData['name'], 
            "email" => $userData['email'],
            "login" => $userData['login'], 
            "password" => $userData['password'],
            "lang" => $userData['lang'],
            "accessNum" => $userData['accessNum']
        ]
    );
}

function getDataTable(object $connection, string $tableName, int $page, int $perPage = 1): array
{
    $connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $stmt = $connection->query("SELECT * FROM `" . $tableName . "` LIMIT ".$perPage." 
        OFFSET " . ($page-1) * $perPage);
    return $stmt->fetchAll();
}

function getAllDataTable(object $connection, string $tableName): array
{
    $connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $stmt = $connection->query("SELECT * FROM `".$tableName."` ");
    return $stmt->fetchAll();
}

function writeUserDataToTable(string $fileJson, string $blankTxt, string $tableName, object $connection): array
{
    $firstRecord = getDataTable($connection, $tableName, 1);//getting first record 
    if (empty($firstRecord)) {
        //Reading file that consists of users data
        $users = openCreateUsersJson($fileJson, $blankTxt);
        //Defining keys from array $users
        $usersKeys = array_keys($users[0]);
        foreach($users as $user) {
            addUserToTable($user, $connection);
        }
    }
    unset($users);
    return getAllDataTable($connection, $tableName);
}

//Function for check login and password, and displaying user data
function checkLoginPsw(object $connection, string $login, string $password): array
{
    $password = encryptPass($password);
    $connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $stmt = $connection->prepare("
        SELECT * FROM 
            `users` 
        WHERE 
            `login` = :login
        AND 
            `password` = :password
    ");
    $stmt->execute(
        [
            "login" => $login,
            "password" => $password
        ]
    );
    $userData = $stmt->fetch();
    return (!empty($userData) ? $userData : []);
}

//Function for increment counter of site access for $i-user
function accessStatistics(object $connection, array $userData, string $login, string $password): array
{
    $stmt = $connection->prepare("
        UPDATE 
            `users`
        SET
            `accessNum` = :accessNum
        WHERE 
            `login` = :login
    ");
    $stmt->execute(
        [
            "accessNum" => (++$userData['accessNum']), 
            "login" => $userData['login']
        ]
    );
    //$userData = checkLoginPsw($connection, $login, $password);
    return $userData;
}

//Function that checks user email displays user login
function checkEmail(object $connection, string $email): string
{
    $connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $stmt = $connection->prepare("
        SELECT `login` FROM 
            `users` 
        WHERE 
            `email` = :email
    ");
    $stmt->execute(
        [
            "email" => $email
        ]
    );
    $data = $stmt->fetch();
    return (!empty($data) ? $login = $data['login'] : "");
}

//Function for saving new hashing password by email of existed user
function saveNewPassword(object $connection, string $login, string $newPassword): void
{
    $stmt = $connection->prepare("
        UPDATE 
            `users`
        SET
            `password` = :password
        WHERE 
            `login` = :login
    ");
    $stmt->execute(
        [
            "password" => encryptPass($newPassword), 
            "login" => $login
        ]
    );
}

//Function for deleting user by login
function deleteUser (object $connection, string $login): void
{
    $connection->query("DELETE FROM `users` WHERE `login` = '{$login}'");
}

//Function for finding user id by specified column from table `users`
function findUserId (object $connection, string $columnName, string $columnValue): int
{
    $connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $stmt = $connection->query("SELECT `id` FROM `users` WHERE ".$columnName."='".$columnValue."'");
    $id = $stmt->fetch();
    return (!empty($id) ? (int) $id['id'] : 0);
}

//Function for errors processing
function errorsProc(array $errors): void
{
    foreach ($errors as $error) {
        switch ($error) {
            case 1: echo "Login or password is wrong. <br>";
            break;
            case 2: echo "Login or password is empty. <br>";
            break;
            case 3: echo "User with this email isn't exists. <br>";
            break;
            case 4: echo "Field for email is empty. <br>";
            break;
            case 5: echo "User by email wasn't finded. <br>";
            break;
            case 6: echo "New password or confirm password is empty. <br>";
            break;
            case 7: echo "Password wasn't confirmed. <br>";
            break;
            case 8: echo "The length of password must be from 7 to 13 symbols. <br>";
            break;
            case 9: echo "All fields marked with an asterisk must be written. <br>";
            break;
            case 10: echo "User with this login is exists. <br>";
            break;
            case 11: echo "User with this email is exists. <br>";
            break;
        }
    }
}

//Function that displays greeting
function greeting($lang){
    switch($lang){
        case 'en':
            $hello = "Hello, ";
        break;
        case 'ua':
            $hello = "Привіт, ";
        break;
        case 'ru':
            $hello = "Привет, ";
        break;
        case 'de':
            $hello = "Hallo, ";
        break;
        case 'fr':
            $hello = "Bonjour, ";
        break;
        case 'sp':
            $hello = "Hola, ";
        break;
        case 'it':
            $hello = "Ciao, ";
        break;
        default:
            $hello = "Hello, ";
    }
    return $hello;
}

//Function for deleting all cookies
function deleteAllCookies()
{
    $hour = time() - 3600;
    foreach ( $_COOKIE as $key => $val ) {
        setcookie( $key, "", $hour);
    }
}