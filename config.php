<?php
define("ROOT_PATH", dirname(__FILE__));
require_once ROOT_PATH . DIRECTORY_SEPARATOR . "functions.php";

define("DB_NAME", "hw_15");
define("DB_USER", "admin");
define("DB_PASSWORD", "Anton");
try {
    $dsn = "mysql:host=localhost;port=3306;dbname=".DB_NAME.";charset=utf8";
    $pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected successfully";
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}

$sessionid = "s123gsd1";
setcookie("selectedProducts", $sessionid, time()+(60*60*24*3)); //Saving cookies for 3 days

session_start();