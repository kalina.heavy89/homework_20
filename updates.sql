ALTER TABLE `Users` CHANGE `name` `login` varchar(255) NOT NULL;

ALTER TABLE `Users` ADD COLUMN `name` VARCHAR(255) NOT NULL;
# ALTER TABLE `Users` MODIFY COLUMN `name` VARCHAR(255) AFTER `id`;
ALTER TABLE `Users` ADD COLUMN `lang` VARCHAR(255) NOT NULL;
ALTER TABLE `Users` ADD COLUMN `accessNum` INT(11) DEFAULT NULL;

ALTER TABLE `Users` RENAME TO `users`;
ALTER TABLE `Cart_items` RENAME TO `cart_items`;
ALTER TABLE `Carts` RENAME TO `carts`;
ALTER TABLE `Categories` RENAME TO `categories`;
ALTER TABLE `Products` RENAME TO `products`;