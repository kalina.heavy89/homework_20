<?php
    require_once "config.php";
    $directory = ROOT_PATH . DIRECTORY_SEPARATOR;
    require_once "functions.php";

?>

<!DOCTYPE>
<html>
<head>
    <meta charset="utf-8">
    <title>Site entrance</title>
    <style>
         /* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
  padding-top: 60px;
}

/* Modal Content/Box */
.modal-content {
  background-color: #fefefe;
  margin: 5px auto; /* 15% from the top and centered */
  border: 5px solid #888;
  padding: 10px;
  width: 50%; /* Could be more or less, depending on screen size */
  text-align: center;
}

    </style>
</head>
<body>
    <div>
        <?php 
            if (!isset($_SESSION['userData'])) { //If userName isn't set
               //emptyLoginPassword ();
              $_SESSION['errors'][] = 2;
              header("location: /hw_20/entrance.php");
              die();
            } else {
                echo greeting($_SESSION['userData']['lang']) . $_SESSION['userData']['name'] . "!"; 
            } ?>
    </div>

    <h1> Site Page </h1>

    <form action = "login_check.php" method = "post">
        <div>
            <input type = "submit" name = "action" value = "Exit" />
        </div>
    </form>

    <!-- Button to open the modal deleting form -->
    <div> Delete account? </div>
    <button onclick="document.getElementById('id01').style.display='block'">Delete</button>
    <!-- The Modal -->
    <div id="id01" class="modal">
        <span onclick="document.getElementById('id01').style.display='none'"
        class="close" title="Close Modal">&times;</span>

        <!-- Modal Content -->
        <form class="modal-content animate" action="login_check.php" method = "post">
            <div class="container">
                <label><b>Are you sure what you want to delete your account?</b></label>
            </div>
            <button type="submit" name = "action" value = "Delete" /> Yes </button>
            <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn"> No </button>
        </form>
    </div>

</body>
</html>